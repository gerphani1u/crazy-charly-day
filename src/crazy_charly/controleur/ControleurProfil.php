<?php
namespace crazy_charly\controleur;

use crazy_charly\model\User;
use crazy_charly\vue\VueProfil;

define("LIST_USERS", "liste des utilisateurs");
define("USR",    "detail de l'utilisateur");

class ControleurProfil{

	public function get_Liste_Membres(){
		$listeMembre= User::orderBy('nom')->get();
		$v    = new VueProfil($listeMembre, LIST_USERS);
		$v->render();
	}

	public function get_Detail_Membre($numero){
		$membre = User::where('id','like',$numero)->get();
		$v    = new VueProfil($membre, USR);
		$v->render();
	}


}
