<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 09/02/2017
 * Time: 09:57
 */


namespace crazy_charly\vue;


use Slim\Slim;

class VueAccueil {

    private $content;

    function __construct($c)
    {
        $this->content = $c;
    }


    function render($id){

        $app =Slim::getInstance();

        $urlMembre = $app-> urlFor('membre');
        $urlLogement = $app->urlFor('logement');
        $urlConnexion = $app->urlFor('connexion');
        $cheminlogo= "web/Logo.png";
        $urlGroupe = $app->urlFor('groupe');
        $urlMonCompte = $app->urlFor('monCompte');
        $urlAccueil = $app->urlFor('accueil');

        $cont ="<p> Erreur </p>";

        switch ($id){
            case 1:
                $cont = $this->accueil();
                break;
            case 2:
                $cont = $this->monCompte();
        }
        $tmp = "Connexion";

        if($_SESSION['id']>0){

            $tmp = "Déconnexion";
            $urlConnexion = $app->urlFor('seDeconnecte');


        }


        $html = <<<END
        
        <!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Un Toit Partagé</title>
	<link rel="stylesheet" href="web/css/bootstrap.css">
	<link rel="stylesheet" href="web/css/Index.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
</head>
<body>
	<header>
		<a class="titre" href="$urlAccueil"><h1>Un Toit Partagé</h1></a>
	</header>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
			<img class="navbar-brand" src="$cheminlogo">
			</div>
			<ul class="nav navbar-nav">
				<li><a href="$urlGroupe">Groupes</a></li>
				<li><a href="$urlMembre"><span class="glyphicon glyphicon-th-list"></span>  Consulter Membres</a></li>
				<li><a href="$urlLogement"><span class="glyphicon glyphicon-th-list"></span>  Consulter Logements</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class="active"><a href="$urlMonCompte"><span class="glyphicon glyphicon-user"></span>  Mon Compte</a></li>
				<li class="active"><a href="$urlConnexion"><span class="glyphicon glyphicon-log-in"></span> $tmp</a></li>
			</ul>
		</div>
	</nav>
	<article>
	<div>
$cont
		</div>
		</article>
		<footer>
			<p>Info Asso</p>
		</footer>





	</body>
	</html>


END;

        echo $html;

    }


    function accueil(){


        $html = <<<END
<h3>Bienvenue sur le site de l'Association <b>"Un Toit Partagé"</b></h3>
		<p>Ce site a pour but de trouver des personnes afin de créer une collocation. En vous inscrivant au site, 
			vous pourrez choisir un logement et inviter des membres et former un groupe qui sera étudié par les gestionnaires de l'Association. </p>
END;



        return $html;



    }

    function monCompte(){

        $res = '<div class="jambon">';
            $res.="<p class = 'jako'>";
            $res.='<img class ="pizza" src="img/user/'.$_SESSION['id'].'.jpg"/>';
            $res.='</br>';
            $res.=" ".$this->content->nom;
            $res.=" ".$this->content->message;
        $res.='</p>';
        return $res;


    }


}