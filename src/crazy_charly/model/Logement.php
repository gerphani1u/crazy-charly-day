<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 09/02/2017
 * Time: 09:47
 */

namespace crazy_charly\model;


class Logement extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'logement';
    protected $primaryKey = 'id';
    public $timestamps = false;
}