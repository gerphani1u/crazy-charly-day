<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 09/02/2017
 * Time: 09:55
 */


namespace crazy_charly\controleur;

use crazy_charly\model\User;
use crazy_charly\vue\VueAccueil;
use Slim\Slim;

class ControleurAccueil {


    function accueil(){

        $vue = new VueAccueil("null");
        $html = $vue->render(1);

        return $html;
    }

    function monCompte(){

        if($_SESSION['id']>0){

            $req = User::where('id','like',$_SESSION['id'])->first();

            $vue = new VueAccueil($req);
            $html = $vue->render(2);
            return $html;
        }

        $app = Slim::getInstance();
        $app -> redirect($app->urlFor('connexion'));

    }
}