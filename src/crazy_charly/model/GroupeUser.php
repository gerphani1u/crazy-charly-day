<?php
namespace crazy_charly\model;


class GroupeUser extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'groupeUser';
    protected $primaryKey = 'idUser';
    public $timestamps = false;

}

?>