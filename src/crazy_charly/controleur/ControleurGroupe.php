<?php
namespace crazy_charly\controleur;

use crazy_charly\model\Groupe;
use crazy_charly\model\User;
use crazy_charly\vue\VueGroupe;
use Slim\Slim;

class ControleurGroupe {

	function nouveauGroupe($descr){
		if($_SESSION['id']>0 && !isset($_SESSION['groupe'])){
			$_SESSION['groupe'] = [$_SESSION['id']];
			$_SESSION['description'] = $descr;
		}
		$app= Slim ::getInstance();
		$app->redirect($app->urlFor('groupe'));
	}

	function ajouterMembre($id){
		if($_SESSION['id']>0 && !isset($_SESSION['groupe'])){
			$_SESSION['groupe'][] = $id;
		}
		$app= Slim ::getInstance();
		$app->redirect($app->urlFor('groupe'));
	}

	function rechercherGroupe(){
		$aUnGroupe = false;
		$liste = User::where('id','like',$_SESSION['id'])->get();
		foreach ($liste as $user) {
			if($user->idGroupe!=0){
				$aUnGroupe=true;
			}
		}
		return $aUnGroupe;
	}

	function ajouterLogement($logement){
		if($_SESSION['id']>0 && !isset($_SESSION['groupe'])){
			$_SESSION['logement']=$logement;
		}
	}

	function validerGroupe(){
		if($_SESSION['id']>0 && $this->rechercherGroupe()==false){
			$groupe = new Groupe();
			$groupe->description = $_SESSION['description'];
			$groupe->save();
			$liste = User::where('id','like',$_SESSION['id'])->get();
			foreach ($liste as $user) {
				$user->idGroupe = $groupe->id;
				$user->save();
			}

		}
		$app = Slim::getInstance();
		$app->redirect($app->urlFor('groupe'));
	}

	function ajouterUtlisateur(){

	}

	function afficher(){
		$vue = new VueGroupe();
		$vue->render();
	}

}

?>