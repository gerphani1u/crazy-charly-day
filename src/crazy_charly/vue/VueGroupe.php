<?php

namespace crazy_charly\vue;
use Slim\Slim;

class VueGroupe{


	public function render(){
		$app =Slim::getInstance();
		$urlMembre = $app-> urlFor('membre');
        $urlLogement = $app->urlFor('logement');
        $urlConnexion = $app->urlFor('connexion');
        $urlGroupe = $app->urlFor('groupe');
        $urlAccueil = $app->urlFor('accueil');
        $urlMonCompte = $app->urlFor('monCompte');

        $cheminbout = "web/css/bootstrap.css";
        $chemincss = "web/css/Index.css";
        $cheminlogo= "web/Logo.png";

		$descriptionGroupe="";
		$nbMembre = "";
		if(isset($_SESSION['groupe'])){
			$descriptionGroupe = $_SESSION['description'];
			$nbMembre = count($_SESSION['groupe']);
		}

        $tmp = "Connexion";

        if($_SESSION['id']>0){

            $tmp = "Déconnexion";
            $urlConnexion = $app->urlFor('seDeconnecte');


        }
		$html="";
		if($descriptionGroupe!=""){
			$html=<<<EOT
       
        <!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Un Toit Partagé</title>
	<link rel="stylesheet" href="web/css/bootstrap.css">
	<link rel="stylesheet" href="web/css/Index.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
</head>
<body>
	<header>
		<a class="titre" href="$urlAccueil"><h1>Un Toit Partagé</h1></a>
	</header>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
			<img class="navbar-brand" src="$cheminlogo">
			</div>
			<ul class="nav navbar-nav">
				<li><a href="$urlGroupe">Groupes</a></li>
				<li><a href="$urlMembre"><span class="glyphicon glyphicon-th-list"></span>  Consulter Membres</a></li>
				<li><a href="$urlLogement"><span class="glyphicon glyphicon-th-list"></span>  Consulter Logements</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class="active"><a href="$urlMonCompte"><span class="glyphicon glyphicon-user"></span>  Mon Compte</a></li>
				<li class="active"><a href="$urlConnexion"><span class="glyphicon glyphicon-log-in"></span>  $tmp</a></li>
			</ul>
		</div>
	</nav>
	<article>
	<div>
	<p class="titreGroupe">Affichage des caractéristiques du groupe : </p>

		<p class="descr">Texte de description : $descriptionGroupe </p>
		<p class="descr">Nombre de membres : $nbMembre </p>
		</div>
		</article>
		<footer>
			<p>Info Asso</p>
		</footer>





	</body>
	</html>
EOT;
		}else{
			
		$html=<<<EOT
		    <!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Un Toit Partagé</title>
	<link rel="stylesheet" href="web/css/bootstrap.css">
	<link rel="stylesheet" href="web/css/Index.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
</head>
<body>
	<header>
		<a class="titre" href="$urlAccueil"><h1>Un Toit Partagé</h1></a>
	</header>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
			<img class="navbar-brand" src="$cheminlogo">
			</div>
			<ul class="nav navbar-nav">
				<li><a href="$urlGroupe">Groupes</a></li>
				<li><a href="$urlMembre"><span class="glyphicon glyphicon-th-list"></span>  Consulter Membres</a></li>
				<li><a href="$urlLogement"><span class="glyphicon glyphicon-th-list"></span>  Consulter Logements</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class="active"><a href="$urlMonCompte"><span class="glyphicon glyphicon-user"></span>  Mon Compte</a></li>
				<li class="active"><a href="$urlConnexion"><span class="glyphicon glyphicon-log-in"></span>  $tmp </a></li>
			</ul>
		</div>
	</nav>
	<article>
	<div>
	VUE GROUPE

		<form action="creationGroupe" method="post">
			<input type="text" name="description"><br>
			<input type="submit" value="Creer un groupe">
		</form>
		</div>
		</article>
		<footer>
			<p>Info Asso</p>
		</footer>





	</body>
	</html>

EOT;
}

		echo $html;
	}
}




?>