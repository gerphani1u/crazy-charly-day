<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 09/02/2017
 * Time: 20:16
 */

namespace crazy_charly\vue;

use Slim\Slim;


class VueAuthentification{


    private $liste;

    public function __construct($liste){
        $this->liste=$liste;
    }


    public function render($id){

        $app =Slim::getInstance();

        $urlMembre = $app-> urlFor('membre');
        $urlLogement = $app->urlFor('logement');
        $urlConnexion = $app->urlFor('connexion');
        $urlGroupe = $app->urlFor('groupe');
        $urlAccueil = $app->urlFor('accueil');
        $urlMonCompte = $app->urlFor('monCompte');

        $cont ="<p> Erreur </p>";
        switch($id){
            case 1:
                $cont = $this->afficher_Liste_Membres();
                $cheminbout = "web/css/bootstrap.css";
                $chemincss = "web/css/Index.css";
                $cheminlogo= "web/Logo.png";
                break;
        }

        $tmp = "Connexion";

        if($_SESSION['id']>0){

            $tmp = "Déconnexion";
            $urlConnexion = $app->urlFor('seDeconnecte');


        }


        $html = <<<END
        
        
        
        <!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Un Toit Partagé</title>
	<link rel="stylesheet" href="$cheminbout">
	<link rel="stylesheet" href="$chemincss">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
</head>
<body class="kevin">
	<header>
		<a class="titre" href="$urlAccueil"><h1>Un Toit Partagé</h1></a>
	</header>
	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<img class="navbar-brand" src="$cheminlogo">
			</div>
			<ul class="nav navbar-nav">
				<li><a href="$urlGroupe">Groupes</a></li>
				<li><a href="$urlMembre"><span class="glyphicon glyphicon-th-list"></span>  Consulter Membres</a></li>
				<li><a href="$urlLogement"><span class="glyphicon glyphicon-th-list"></span>  Consulter Logements</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li class="active"><a href="$urlMonCompte"><span class="glyphicon glyphicon-user"></span>  Mon Compte</a></li>
				<li class="active"><a href="$urlConnexion"><span class="glyphicon glyphicon-log-in"></span> $tmp</a></li>
			</ul>
		</div>
	</nav>
	<article>
	<div class="zebi">
$cont
		</div>
		</article>
		<footer>
			<p>Info Asso</p>
		</footer>





	</body>
	</html>


END;

        echo $html;

    }

    public function afficher_Liste_Membres(){
        $res = '<ul id="listeUsr">';
        $app =Slim::getInstance();


        foreach ($this->liste as $user) {
            $url = $app->urlFor('seConnecte',['id' => $user->id]);
            $res .="<li class=thomas>"."<a href=$url> ".' <img src="img/user/'.$user->id.'.jpg"/>'.'<p class="membres">'.$user->id.' - '.$user->nom .'</p></li></a>';
        }
        $res .= '</ul>';
        return $res;
    }




}