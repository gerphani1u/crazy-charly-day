<?php
/**
 * Created by PhpStorm.
 */

require 'vendor/autoload.php';

use crazy_charly\controleur\ControleurAccueil;
use crazy_charly\controleur\ControleurProfil;
use crazy_charly\controleur\ControleurLogement;
use crazy_charly\controleur\ControleurAuthentification;
use crazy_charly\controleur\ControleurGroupe;

$db = new \Illuminate\Database\Capsule\Manager();

$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

$app=new \Slim\Slim;

session_start();



if(!isset($_SESSION['id'])){

    $_SESSION['id'] = -1;
}

//unset($_SESSION['id']);

$app->get('/',function (){ 
	(new ControleurAccueil())->accueil();
})->name('accueil');

$app->get('/membre/:id', function($id){ (new ControleurProfil())->get_Detail_Membre($id);})->name('membreId');

$app->get('/membres', function(){ (new ControleurProfil())->get_Liste_Membres();})->name('membre');

$app->get('/logement', function(){(new ControleurLogement())->listerLogement();})->name('logement');

$app->get('/logement/:id', function($id){(new ControleurLogement())->get_Detail_Logement($id);})->name('logement_detail');

$app->get('/connexion', function(){ (new ControleurAuthentification())->getUsers();})->name('connexion');

$app->get('/seConnecte/:id',function ($id){ (new ControleurAuthentification())->seConnecte($id);})->name('seConnecte');

$app->get('/seDeconnecte',function (){ (new ControleurAuthentification())->seDeconnecte();})->name('seDeconnecte');

$app->get('/groupe',function(){(new ControleurGroupe())->afficher();})->name('groupe');

$app->post('/creationGroupe' , function(){ (new ControleurGroupe())->nouveauGroupe($_POST['description']);})->name('creationGroupe');

$app->get('/monCompte',function (){ (new ControleurAccueil())->monCompte();})->name('monCompte');

$app->run();






