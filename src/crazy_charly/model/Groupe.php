<?php
namespace crazy_charly\model;


class Groupe extends \Illuminate\Database\Eloquent\Model {

    protected $table = 'groupe';
    protected $primaryKey = 'id';
    public $timestamps = false;

}

?>