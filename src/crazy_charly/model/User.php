<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 09/02/2017
 * Time: 09:43
 */


namespace crazy_charly\model;


class User extends \Illuminate\Database\Eloquent\Model {


    protected $table = 'user';
    protected $primaryKey = 'id';
    public $timestamps = false;
}