<?php
/**
 * Created by PhpStorm.
 * User: Thomas
 * Date: 09/02/2017
 * Time: 09:55
 */


namespace crazy_charly\controleur;

use crazy_charly\model\User as User;
use crazy_charly\vue\VueAuthentification;
use Slim\Slim;

class ControleurAuthentification {


    function getUsers(){
        $liste = User::select()->get();

        $vue = new VueAuthentification($liste);
        $vue->render(1);
    }


    function seConnecte($id){

        $_SESSION['id'] = $id;

        $app = Slim::getInstance();

        $app -> redirect($app->urlFor('accueil'));


    }


    function seDeconnecte(){

        $_SESSION['id'] = -1;
        unset($_SESSION['groupe']);
        unset($_SESSION['description']);
        $app = Slim::getInstance();
        $app -> redirect($app->urlFor('accueil'));
    }


}